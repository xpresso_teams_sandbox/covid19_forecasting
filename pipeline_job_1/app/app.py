"""
This is the implementation of data preparation for sklearn
"""

import sys
import logging
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np
from sklearn.preprocessing import LabelEncoder,OneHotEncoder
from sklearn.ensemble import RandomForestRegressor
from numpy import loadtxt
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.metrics import mean_squared_log_error
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger

__author__ = "### Author ###"

logger = XprLogger("pipeline_job_1",level=logging.INFO)


class PipelineJob1(AbstractPipelineComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self):
        super().__init__(name="PipelineJob1")
        """ Initialize all the required constansts and data her """

    def start(self, run_name):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            run_name: xpresso run name which is used by base class to identify
               the current run. It must be passed. While running as pipeline,
               Xpresso automatically adds it.

        """
        super().start(xpresso_run_name=run_name)

        def feature_engineering(df):
            df['Month'] = df['Date'].dt.month
            df['Day'] = df['Date'].dt.day
            df['Day_Week'] = df['Date'].dt.dayofweek
            df['quarter'] = df['Date'].dt.quarter
            df['dayofyear'] = df['Date'].dt.dayofyear
            df['weekofyear'] = df['Date'].dt.weekofyear

            #     enc = LabelEncoder()
            #     df['Country'] = enc.fit_transform(df['Country'])

            #     one_hot_workclass =pd.get_dummies(df.Country)

            #     print(one_hot_workclass)

            df['conf_rollmean_7'] = df.groupby(['Country'])['Confirmed'].transform(lambda x: x.rolling(7).mean())

            df['conf_rollstd_7'] = df.groupby(['Country'])['Confirmed'].transform(lambda x: x.rolling(7).std())

            df.fillna(0, inplace=True)

            df['Date'] = df['Date'].apply(lambda x: datetime.strftime(x, '%Y-%m-%d'))

            # Format date
            df['Date'] = df['Date'].apply(lambda x: x.replace("-", ""))
            df['Date'] = df['Date'].astype(int)

            min_date = df['Date'].min()
            df['DateDiff'] = df['Date'].apply(lambda x: x - min_date)

            df = pd.concat([df, pd.get_dummies(df['Country'])], axis=1)

            df = pd.concat([df, pd.get_dummies(df['Province'])], axis=1)
            #     print(df)
            return df

        def RMSLError(y_test, predictions):
            return np.sqrt(mean_squared_log_error(y_test, predictions))

        def rf(dataset):
            features = ['Province', 'Country', 'Lat', 'Long', 'Date', 'Confirmed', 'Month',
                        'Day', 'Day_Week', 'quarter', 'dayofyear', 'weekofyear',
                        'conf_rollmean_7', 'conf_rollstd_7', 'DateDiff']

            #     train,test=train_test_split(data,7)
            y = dataset['Confirmed']
            #         np.
            np.squeeze(y, axis=0)
            x = dataset.drop(['Confirmed', 'Province', 'Date', 'weekofyear', 'Country'], axis=1)
            #         print(x.columns)
            #         shuffling to be performed
            X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.3, random_state=42)

            rf = RandomForestRegressor(n_estimators=100, n_jobs=-1, min_samples_leaf=1, random_state=17)

            rf.fit(X_train, y_train)

            predictions = rf.predict(X_test)

            rmsle = RMSLError(y_test, predictions)

            return rmsle

        # === Your start code base goes here ===
        self.completed()

    def send_metrics(self):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        report_status = {
            "status": {"status": "data_preparation"},
            "metric": {"metric_key": 1}
        }
        self.report_status(status=report_status)

    def completed(self, push_exp=False):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned

        """
        # === Your start code base goes here ===
        super().completed(push_exp=push_exp)

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        super().terminate()

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        super().pause()

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        super().restart()


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/app.py

    data_prep = PipelineJob1()
    if len(sys.argv) >= 3:
        data_prep.start(run_name=sys.argv[1],filepath = sys.argv[2] )
    else:
        data_prep.start(run_name="",filepath = None)
