"""
This is a Random Forest model for Covid19 Forecasting
It prints rmsle value
"""
__author__ = "Rohit Raj"

from datetime import datetime, timedelta
import sys
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np
from sklearn.preprocessing import LabelEncoder,OneHotEncoder
from sklearn.ensemble import RandomForestRegressor
from numpy import loadtxt
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.metrics import mean_squared_log_error
# from sklearn.preprocessing import LabelEncoder
import logging
from xpresso.ai.core.logging.xpr_log import XprLogger




def feature_engineering(df):
    df['Month'] = df['Date'].dt.month
    df['Day'] = df['Date'].dt.day
    df['Day_Week'] = df['Date'].dt.dayofweek
    df['quarter'] = df['Date'].dt.quarter
    df['dayofyear'] = df['Date'].dt.dayofyear
    df['weekofyear'] = df['Date'].dt.weekofyear
    

#     enc = LabelEncoder()
#     df['Country'] = enc.fit_transform(df['Country'])

#     one_hot_workclass =pd.get_dummies(df.Country) 
    
#     print(one_hot_workclass)

    
    
    df['conf_rollmean_7'] = df.groupby(['Country'])['Confirmed'].transform(lambda x: x.rolling(7).mean())
    
    df['conf_rollstd_7'] = df.groupby(['Country'])['Confirmed'].transform(lambda x: x.rolling(7).std())
    
    df.fillna(0,inplace=True)
    
    df['Date'] = df['Date'].apply(lambda x: datetime.strftime(x, '%Y-%m-%d'))

    
    # Format date
    df['Date'] = df['Date'].apply(lambda x: x.replace("-",""))
    df['Date']  = df['Date'].astype(int)
    
    min_date = df['Date'].min()
    df['DateDiff'] = df['Date'].apply(lambda x: x- min_date)
    
    df=pd.concat([df,pd.get_dummies(df['Country'])], axis=1)
    
    df=pd.concat([df,pd.get_dummies(df['Province'])], axis=1)
#     print(df)
    return df

    
    
    
    



def RMSLError(y_test, predictions):
    return np.sqrt(mean_squared_log_error(y_test, predictions))



def rf(dataset):


        features=['Province', 'Country', 'Lat', 'Long', 'Date', 'Confirmed', 'Month',
                   'Day', 'Day_Week', 'quarter', 'dayofyear', 'weekofyear',
                   'conf_rollmean_7', 'conf_rollstd_7','DateDiff']
 
#     train,test=train_test_split(data,7)
        y = dataset['Confirmed']
#         np.
        np.squeeze(y, axis=0)
        x = dataset.drop(['Confirmed','Province','Date','weekofyear','Country'],axis=1)
#         print(x.columns)
#         shuffling to be performed 
        X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.3, random_state=42)
        
        rf = RandomForestRegressor(n_estimators=100, n_jobs= -1, min_samples_leaf=1, random_state=17)

        rf.fit(X_train,y_train)
        
        
        predictions = rf.predict(X_test)
        
        rmsle = RMSLError(y_test,predictions)
        
        return rmsle

    




if __name__ == '__main__':
    logger = XprLogger(level= logging.DEBUG)
    filepath =sys.argv[2]
    dataset = pd.read_excel(filepath)

    dataset.fillna("Unknown", inplace=True)

    dataset = feature_engineering(dataset)

    val= rf(dataset)
    logger.debug("RMSLError is {}".format(str(val)))




